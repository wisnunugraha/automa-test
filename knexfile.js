// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */ require("dotenv").config();
const { DBURL, DBPORT, DBUSER, DBPASSWORD, DBNAME } = process.env;

module.exports = {
  development: {
    client: "pg",
    connection: {
      host: DBURL,
      port: DBPORT,
      user: DBUSER,
      password: DBPASSWORD,
      database: DBNAME,
    },
    migrations: {
      directory: "./migrations",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },

  production: {
    client: "pg",
    connection: {
      host: DBURL,
      port: DBPORT,
      user: DBUSER,
      password: DBPASSWORD,
      database: DBNAME,
    },
    migrations: {
      directory: "./migrations",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
};
