const productsSubscribe = require("./products.subscribe");

module.exports = {
  productsSubscribe,
};
