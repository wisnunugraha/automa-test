const { productsHelper } = require("../../helper");
exports.productsSubscribe = async ({ topic, payload }) => {
  console.log("Received Message:", topic, payload);
  if (payload.topic === "update") {
    delete payload.topic;
    productsHelper.updateProductsHelper(payload);
  }
};
