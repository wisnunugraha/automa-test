const { productsSubscribe } = require("./products");

module.exports = {
  productsSubscribe,
};
