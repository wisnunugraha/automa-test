/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("products", function (table) {
    table.increments("id").primary();
    table.string("name", 255).notNullable();
    table.string("description", 1000).notNullable();
    table.decimal("price", 10, 2).notNullable(); // Decimal with 10 digits and 2 decimal places
    table.integer("quantity").notNullable();
    // Add other columns as needed
    table.timestamps(true, true); // Adds created_at and updated_at columns
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("products");
};
