/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("users", function (table) {
    table.increments("id").primary();
    table.string("username", 100).notNullable().unique();
    table.string("password",255).notNullable();
    table.integer("type").notNullable().defaultTo(0);
    table.integer("status").notNullable().defaultTo(0);
    table.boolean("active").notNullable().defaultTo(true);
    // Add other fields as needed
    table.timestamps(true, true); // Adds created_at and updated_at columns
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("users");
};
