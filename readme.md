# Automatest

Problem: Building a Product Inventory API with Knex.js (https://github.com/knex/knex), MQTTIntegration, and User Authentication in Node JS

You are tasked with building a backend API for a product inventory system. The API should allowusers to perform CRUD operations (Create, Read, Update, Delete) on product records stored in aMySQL database using Knex.js as the query builder. Additionally, the API should accept input viaMQTT to update product quantities. User authentication is required to access the API endpoints

## Authors

- [@wisnunugraha](https://www.gitlab.com/wisnunugraha)

## API Reference

#### Auth API Login

```http
  POST /api/v1/auth/login
```

| Parameter.  | Type     | Description                |
| :---------- | :------- | :------------------------- |
| `usersname` | `string` | **Required**. Your API key |
| `password`  | `string` | **Required**. Your API key |

#### Auth API Register

```http
  POST /api/v1/auth/login
```

| Parameter.  | Type     | Description                |
| :---------- | :------- | :------------------------- |
| `usersname` | `string` | **Required**. Your API key |
| `password`  | `string` | **Required**. Your API key |

#### Products API All

```http
  GET /api/v1/products
```

| Parameter | Type | Description      |
| :-------- | :--- | :--------------- |
|           |      | Showing all data |

#### Products API ID

```http
  GET /api/v1/products/:id
```

| Parameter.  | Type     | Description                |
| :---------- | :------- | :------------------------- |
| `params:id` | `number` | **Required**. Your API key |

#### Products API Update

```http
  PUT /api/v1/products
```

| Parameter.      | Type     | Description                |
| :-------------- | :------- | :------------------------- |
| `body:id`       | `number` | **Required**. Your API key |
| `body:name`     | `string` | **Required**. Your API key |
| `body:price`    | `number` | **Required**. Your API key |
| `body:quantity` | `number` | **Required**. Your API key |

#### Products API ID

```http
  DELETE /api/v1/products/:id
```

| Parameter.  | Type     | Description                |
| :---------- | :------- | :------------------------- |
| `params:id` | `number` | **Required**. Your API key |

## Run This Project

run this project

```bash
  create db `automatest`
```

clone this project and run npm install

```bash
  clone this project and cd to project
```

```bash
  npm install
```

you must check configurations on .env of database, port and etc.

```bash
  .env folder
```

before run project, you must migrations table, you need step by step for migrations

```bash
  npm install knex -g
```

```bash
  knex migrate:latest
```

run under development

```bash
  npm run dev
```

run under productions

```bash
  npm run start
```

import postman

```bash
  file Postman Automa - Test.postman_collection.json
```
