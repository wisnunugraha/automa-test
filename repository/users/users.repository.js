const _ = require("../../config/db.config");
class usersRepository {
  getFirstUsersRepository = async (params) => {
    let response;
    if (params.id) {
      response = await _.select().from("users").where("id", params.id).first();
    }
    if (params.usersname) {
      response = await _.select()
        .from("users")
        .where("username", params.usersname)
        .first();
    }

    if (response)
      return {
        status: true,
        response: response,
      };
    return {
      status: false,
      response: null,
    };
  };
  getallUsersRepository = async () => {
    const response = await _.select().from("users");
    if (response)
      return {
        status: true,
        response: response,
      };
    return {
      status: false,
      response: null,
    };
  };
  postUsersRepository = async (params) => {
    try {
      await _.insert(params).into("users");
      return {
        status: true,
        response: "",
        messages: "",
      };
    } catch (e) {
      console.log(e);
      return {
        status: false,
        response: e.message,
        messages: e,
      };
    }
  };
  updateUsersRepository = async ({ data, users_id }) => {
    try {
      await _.update(data).from("users").where("id", users_id);
      return {
        status: true,
        response: "",
        messages: "",
      };
    } catch (e) {
      console.log(e);
      return {
        status: false,
        response: e.message,
        messages: e,
      };
    }
  };
  deletedUsersRepository = async ({ users_id }) => {
    try {
      await _.select().from("users").where("id", users_id).del();
      return {
        status: true,
        response: "",
        messages: "",
      };
    } catch (e) {
      console.log(e);
      return {
        status: false,
        response: e.message,
        messages: e,
      };
    }
  };
}

module.exports = new usersRepository();
