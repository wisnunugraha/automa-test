const _ = require("../../config/db.config");
class productsRepository {
  getFirstProductsRepository = async (params) => {
    const response = await _.select()
      .from("products")
      .where("id", params.id)
      .first();
    if (response)
      return {
        status: true,
        response: response,
      };
    return {
      status: false,
      response: null,
      messages: "Sorry product not found, try another id !",
    };
  };
  getListProductsRepository = async (params) => {
    let data = `where name ilike '%%'`;
    let sort = `order by id asc`;
    if (params.name) {
      data = `where name ilike '%${params.name}%'`;
    }
    if (Number(params.price)) {
      if (Number(params.price) === 1) {
        sort = "order by price asc";
      }
      if (Number(params.price) === 2) {
        sort = "order by price desc";
      }
    }
    if (Number(params.sort)) {
      if (Number(params.sort) === 1) {
        sort = "order by name asc";
      }
      if (Number(params.sort) === 2) {
        sort = "order by name desc";
      }
    }
    const response = await _.raw(`select * from products ${data} ${sort}`);
    if (response.rows.length > 0)
      return {
        status: true,
        response: response.rows,
      };
    return {
      status: false,
      response: null,
      messages: "Sorry product still empty, please create first!",
    };
  };
  postProductsRepository = async (params) => {
    try {
      await _.insert(params).into("products");
      return {
        status: true,
        response: params,
        messages: "Successfully add products",
      };
    } catch (e) {
      return {
        status: false,
        response: e.message,
        messages: e,
      };
    }
  };
  updateProductsRepository = async ({ data, products_id }) => {
    try {
      await _("products").update(data).where("id", products_id);
      return {
        status: true,
        response: data,
        messages: "Successfully  update data",
      };
    } catch (e) {
      return {
        status: false,
        response: e.message,
        messages: e,
      };
    }
  };
  deletedProductsRepository = async ({ products_id }) => {
    try {
      await _.select().from("products").where("id", products_id).del();
      return {
        status: true,
      };
    } catch (e) {
      return {
        status: false,
        response: e.message,
        messages: e,
      };
    }
  };
}

module.exports = new productsRepository();
