const { productsRepository } = require("./products");
const { usersRepository } = require("./users");
module.exports = {
  productsRepository,
  usersRepository,
};
