const mqtt = require("mqtt");
const { productsSubscribe } = require("../subcribers");
let mqttClient;
require("dotenv").config();
const { MQTTURL, MQTTPORT, MQTTUSERS, MQTTPASSWORD, MQTTPRODUCTS } =
  process.env;
const clientId = "client" + Math.random().toString(36).substring(7);

const options = {
  host: MQTTURL,
  clientId: clientId,
  port: MQTTPORT,
  protocol: "mqtts",
  username: MQTTUSERS,
  password: MQTTPASSWORD,
};

mqttClient = mqtt.connect(options);

mqttClient.on("error", (err) => {
  console.log("Error: ", err);
  mqttClient.end();
});

mqttClient.on("reconnect", () => {
  console.log("Reconnecting...");
});

mqttClient.on("connect", () => {
  console.log("Client connected:" + clientId);
});

// Received Message
mqttClient.on("message", (topic, message, packet) => {
  console.log(
    "Received Message: " + message.toString() + "\nOn topic: " + topic
  );
  const payload = JSON.parse(message.toString());
  if (`'${MQTTPRODUCTS}'` === topic) {
    productsSubscribe.productsSubscribe({ topic, payload });
  }
});

module.exports = mqttClient;
