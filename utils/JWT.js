const jwt = require("jsonwebtoken");

exports.JWTGenerate = (params) => {
  //Generate JWT
  const token = jwt.sign({ id: params.id }, process.env.JWTSECRET, {
    expiresIn: process.env.JWTSECRETEXPIRATION,
  });
  return token;
};

exports.JWTRefreshGenerate = (params) => {
  //Generate JWT
  const token = jwt.sign({ id: params.id }, process.env.JWTSECRETREFRESH, {
    expiresIn: process.env.JWTSECRETREFRESHEXPIRATION,
  });
  return token;
};
