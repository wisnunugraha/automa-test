const CryptoJS = require("crypto-js");
const randomstring = require('randomstring');

exports.securityDecrypt = (data, key) => {
    const decryptParams = CryptoJS.AES.decrypt(data, key);
    const decryptedData = JSON.parse(decryptParams.toString(CryptoJS.enc.Utf8));
    return decryptedData;
}

exports.securityEncrypt = (params) => {
    let key = randomstring.generate();
    // Encrypt
    const securityEncrypt = CryptoJS.AES.encrypt(JSON.stringify(params), key).toString();
    return { data: securityEncrypt, key: key };
}

exports.Base64 = (params) => {
    // PROCESS
    const encodedWord = CryptoJS.enc.Utf8.parse(params); // encodedWord Array object
    const encoded = CryptoJS.enc.Base64.stringify(encodedWord); // string: 'NzUzMjI1NDE='
    return encoded;
}