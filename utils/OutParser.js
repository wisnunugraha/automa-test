/**
 *
 * Out Parser {Success}
 * Response
 *    - Status
 *    - Code
 *    - Data
 *    - Messages
 *    - Key
 * Will Be Global Using
 * !important
 *
 */

//------------------------------------//
exports.OutSuccess = (data = null, messages = null, key = null) => {
  return {
    status: true,
    code: 200,
    data: data,
    messages: messages,
    key: key,
  };
};

//------------------------------------//
/**
 *
 * Out Parser {Error}
 * Response
 *    - Status
 *    - Code
 *    - Data
 *    - Messages
 *    - Key
 * Will Be Global Using
 * !important
 *
 */

exports.OutError = (data = null, messages = null, key = null) => {
  return {
    status: false,
    code: 201,
    data: data,
    messages: messages,
    key: key,
  };
};

//------------------------------------//
/**
 *
 * Out Parser {Validation}
 * Response
 *    - Status
 *    - Code
 *    - Data
 *    - Messages
 *    - Key
 * Will Be Global Using
 * !important
 *
 */
exports.OutValidation = (data = null, messages = null, key = null) => {
  return {
    status: false,
    code: 202,
    data: data,
    messages: messages,
    key: key,
  };
};

//------------------------------------//
/**
 *
 * Out Parser {UnAuthorization}
 * Response
 *    - Status
 *    - Code
 *    - Data
 *    - Messages
 *    - Key
 * Will Be Global Using
 * !important
 *
 */
exports.OutUnAuthorization = (data = null, messages = null, key = null) => {
  return {
    status: false,
    code: 203,
    data: data,
    messages: messages,
    key: key,
  };
};

exports.OutSuccessPublisher = (topic = null, message = null) => {
  return {
    topic: topic,
    message: message,
  };
};
