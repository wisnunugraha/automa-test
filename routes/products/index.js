const express = require("express");
const router = express.Router();
const Products = require("./products.router");

router.use("/", Products);
module.exports = router;
