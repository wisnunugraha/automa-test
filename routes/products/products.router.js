const express = require("express");
const router = express.Router();
const { productsController } = require("../../app/");

router.get("/products", productsController.listProductsController);
router.get("/products/:id", productsController.detailsProductsController);
router.post("/products", productsController.postProductsController);
router.put("/products", productsController.updateProductsController);
router.delete("/products/:id", productsController.deletedProductsController);

module.exports = router;
