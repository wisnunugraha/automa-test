const express = require("express");
const router = express.Router();
const Auth = require("./auth");
const Products = require("./products");
const { JWTVerify } = require("../middleware");

router.use("/auth", Auth);
router.use("/product", JWTVerify, Products);

module.exports = router;
