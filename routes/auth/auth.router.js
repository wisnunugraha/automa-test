const express = require("express");
const router = express.Router();
const { authController } = require("../../app/");

router.post("/login", authController.loginAuthController);
router.post("/register", authController.regiserAuthController);

module.exports = router;
