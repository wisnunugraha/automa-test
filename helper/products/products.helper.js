const { productsRepository } = require("../../repository");
class productsHelper {
  listProductsHelper = async (params) => {
    return await productsRepository.getListProductsRepository(params);
  };
  postProductsHelper = async (params) => {
    return await productsRepository.postProductsRepository(params);
  };
  detailProductsHelper = async (params) => {
    return await productsRepository.getFirstProductsRepository(params);
  };
  updateProductsHelper = async (params) => {
    const products_id = params.products_id;
    delete params.products_id;
    return await productsRepository.updateProductsRepository({
      data: params,
      products_id,
    });
  };
  deletedProductsHelper = async (params) => {
    return await productsRepository.deletedProductsRepository({
      products_id: params.id,
    });
  };
}

module.exports = new productsHelper();
