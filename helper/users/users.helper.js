const { usersRepository } = require("../../repository");
class usersHelper {
  usersFirstUsersHelper = async (params) => {
    return await usersRepository.getFirstUsersRepository(params);
  };
  usersPostUsersHelper = async (params) => {
    return await usersRepository.postUsersRepository(params);
  };
}

module.exports = new usersHelper();
