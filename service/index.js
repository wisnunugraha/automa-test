const { authService } = require("./auth");
const { productsService } = require("./products");
module.exports = {
  authService,
  productsService
};
