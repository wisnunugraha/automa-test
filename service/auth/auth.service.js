const { usersHelper } = require("../../helper");
const {
  Bcrypt: { PasswordEncrypt, PasswordCompare },
  JWT: { JWTGenerate, JWTRefreshGenerate },
  Uuid: { uuids },
} = require("../../utils");
class authService {
  authLoginService = async ({ usersname, password }) => {
    // Find users by usernames
    const users = await usersHelper.usersFirstUsersHelper({
      usersname: usersname,
    });
    const user = users.response;
    // Valdiate response of users exist or not exist
    if (users.status === false)
      return {
        status: false,
        response: null,
        messages: "Sorry, username not corectly, please try again!",
      };
    // Validate password with utils comapare encrypt
    const validatePassword = await PasswordCompare(password, user.password);

    // Validate response password not corectly
    if (validatePassword.status === false)
      return {
        status: false,
        response: null,
        messages: "Sorry, password not corectly please try again!",
      };
    // Generate all tokens such as JWT and Refresh
    const JWTTokens = JWTGenerate({ id: user.id });
    const JWTRefreshTokens = JWTRefreshGenerate({ id: user.id });
    // Delete data privacy
    delete user.password;
    delete user.updated_at;
    return {
      status: true,
      response: {
        users: user,
        token: {
          token: JWTTokens,
          refresh: JWTRefreshTokens,
        },
      },
      messages: "Successfully login",
    };
  };
  authRegisterService = async ({ usersname, password }) => {
    // Find users by usernames
    const users = await usersHelper.usersFirstUsersHelper({
      usersname: usersname,
    });
    console.log(users);
    // Valdiate response of users exist or not exist
    if (users.status === true)
      return {
        status: false,
        response: null,
        messages: "Sorry, username exist, please try another username again!",
      };
    // Validate password with utils encrypt
    const validatePassword = await PasswordEncrypt(password);

    // Initiate PArams data post save users
    const usersPost = {
      username: usersname,
      password: validatePassword,
      created_at: new Date(),
    };
    // Save with trycatch
    const usersResponse = await usersHelper.usersPostUsersHelper(usersPost);
    if (usersResponse.status === false) return usersResponse;
    // Generate all tokens such as JWT and Refresh
    const JWTTokens = JWTGenerate({ id: usersPost.id });
    const JWTRefreshTokens = JWTRefreshGenerate({ id: usersPost.id });
    // Delete data privacy
    delete usersPost.password;
    delete usersPost.updated_at;
    return {
      status: true,
      response: {
        users: usersPost,
        token: {
          token: JWTTokens,
          refresh: JWTRefreshTokens,
        },
      },
      messages: "Successfully login",
    };
  };
}

module.exports = new authService();
