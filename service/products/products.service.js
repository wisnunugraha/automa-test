const { productsHelper } = require("../../helper");
const {
  OutParser: { OutSuccessPublisher },
} = require("../../utils");
const { MQTTPublishAPI } = require("../../api");
const { MQTTPRODUCTS } = process.env;
const {
  Bcrypt: { PasswordEncrypt, PasswordCompare },
  JWT: { JWTGenerate, JWTRefreshGenerate },
  Uuid: { uuids },
} = require("../../utils");
class productsService {
  listProductsService = async (params) => {
    const response = await productsHelper.listProductsHelper(params);
    return response;
  };
  postProductsService = async (params) => {
    if (Number(params.price) < 0) {
      return {
        status: false,
        response: params,
        messages: "Sorry, price can not under 0",
      };
    }
    if (Number(params.quantity) < 0) {
      return {
        status: false,
        response: params,
        messages: "Sorry, quantity can not under 0",
      };
    }

    const response = await productsHelper.postProductsHelper(params);
    return response;
  };
  detailProductsService = async (params) => {
    const response = await productsHelper.detailProductsHelper(params);
    return response;
  };
  updateProductsService = async (params) => {
    const response = await productsHelper.detailProductsHelper({
      id: params.id,
    });
    if (response.status === false) return response;
    if (Number(params.price) < 0) {
      return {
        status: false,
        response: params,
        messages: "Sorry, price can not under 0",
      };
    }
    if (Number(params.quantity) < 0) {
      return {
        status: false,
        response: params,
        messages: "Sorry, quantity can not under 0",
      };
    }

    const publisherProducts = {
      topic: "update",
      products_id: params.id,
      name: params.name,
      price: params.price,
      quantity: params.quantity,
      description: params.description,
    };
    MQTTPublishAPI({ topic: `'${MQTTPRODUCTS}'`, messages: publisherProducts });

    return {
      status: true,
      response: params,
      messages: "Successfully update products",
    };
  };

  deleteProductsService = async (params) => {
    const response = await productsHelper.detailProductsHelper(params);
    if (response.status === false) return response;
    await productsHelper.deletedProductsHelper(params);
    return {
      status: true,
      response: null,
      messages: `Successfully deleted products ${response.response.name} `,
    };
  };
}

module.exports = new productsService();
