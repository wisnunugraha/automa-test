const {
  // Login Validations
  productsValidations: {
    productsListValidation,
    productsDetailsValidation,
    productsPostValidation,
    productsUpdateValidation,
  },
} = require("./validations");

const {
  Bcrypt: { PasswordEncrypt, PasswordCompare },
  JWT: { JWTGenerate },
  OutParser: { OutSuccess, OutError, OutUnAuthorization, OutValidation },
  Uuid: { uuids },
} = require("../../utils");
const { productsService } = require("../../service");

class productsController {
  listProductsController = async (req, res, next) => {
    let validation = productsListValidation(req.query);
    if (validation.status == false)
      return res.send(OutValidation(validation, null, "You got validation !"));

    let response = await productsService.listProductsService(req.query);
    if (response.status == true)
      return res.send(OutSuccess(response.response, response.messages));
    return res.send(OutError(response.response, response.messages));
  };

  detailsProductsController = async (req, res, next) => {
    let validation = productsDetailsValidation(req.params);
    if (validation.status == false)
      return res.send(OutValidation(validation, null, "You got validation !"));

    let response = await productsService.detailProductsService(req.params);
    if (response.status == true)
      return res.send(OutSuccess(response.response, response.messages));
    return res.send(OutError(response.response, response.messages));
  };
  postProductsController = async (req, res, next) => {
    let validation = productsPostValidation(req.body);
    if (validation.status == false)
      return res.send(OutValidation(validation, null, "You got validation !"));

    let response = await productsService.postProductsService(req.body);
    if (response.status == true)
      return res.send(OutSuccess(response.response, response.messages));
    return res.send(OutError(response.response, response.messages));
  };

  updateProductsController = async (req, res, next) => {
    let validation = productsUpdateValidation(req.body);
    if (validation.status == false)
      return res.send(OutValidation(validation, null, "You got validation !"));

    let response = await productsService.updateProductsService(req.body);
    if (response.status == true)
      return res.send(OutSuccess(response.response, response.messages));
    return res.send(OutError(response.response, response.messages));
  };

  deletedProductsController = async (req, res, next) => {
    let validation = productsDetailsValidation(req.params);
    if (validation.status == false)
      return res.send(OutValidation(validation, null, "You got validation !"));

    let response = await productsService.deleteProductsService(req.params);
    if (response.status == true)
      return res.send(OutSuccess(response.response, response.messages));
    return res.send(OutError(response.response, response.messages));
  };
}

module.exports = new productsController();
