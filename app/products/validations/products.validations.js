/**
 * Package Service or NPM Here
 */
const Joi = require("joi");

/**
 *
 * Account Validation
 *
 * @param {*} req.body
 * @returns [FALSE || TRUE]
 */

exports.productsListValidation = (req) => {
  let schema = Joi.object().keys({
    name: Joi.string().allow("", null),
    price: Joi.number().allow("", null),
    sort: Joi.number().allow("", null),
  });

  const validation = schema.validate(req);
  if (validation.error)
    return {
      status: false,
      validation: validation.error.details,
    };
  return { status: true };
};
exports.productsDetailsValidation = (req) => {
  let schema = Joi.object({
    id: Joi.number(),
    // .pattern(new RegExp("^[0-9a-fA-F]{24}$"))
  });

  const validation = schema.validate(req);
  if (validation.error)
    return {
      status: false,
      validation: validation.error.details,
    };
  return { status: true };
};
exports.productsPostValidation = (req) => {
  let schema = Joi.object({
    name: Joi.string().required(),
    price: Joi.number().required(),
    quantity: Joi.number().required(),
    description: Joi.string().required(),
  });

  const validation = schema.validate(req);
  if (validation.error)
    return {
      status: false,
      validation: validation.error.details,
    };
  return { status: true };
};
exports.productsUpdateValidation = (req) => {
  let schema = Joi.object({
    id: Joi.number().required(),
    name: Joi.string().required(),
    price: Joi.number().required(),
    quantity: Joi.number().required(),
    description: Joi.string().required(),
  });

  const validation = schema.validate(req);
  if (validation.error)
    return {
      status: false,
      validation: validation.error.details,
    };
  return { status: true };
};
