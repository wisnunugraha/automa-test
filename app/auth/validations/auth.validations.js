/**
 * Package Service or NPM Here
 */
const Joi = require("joi");

/**
 *
 * Account Validation
 *
 * @param {*} req.body
 * @returns [FALSE || TRUE]
 */

exports.authLoginValidation = (req) => {
  let schema = Joi.object({
    usersname: Joi.string().required(),
    password: Joi.string().required(),
  });

  const validation = schema.validate(req);
  if (validation.error)
    return {
      status: false,
      validation: validation.error.details,
    };
  return { status: true };
};
exports.authRegisterValidation = (req) => {
  let schema = Joi.object({
    usersname: Joi.string().required(),
    password: Joi.string().required(),
  });

  const validation = schema.validate(req);
  if (validation.error)
    return {
      status: false,
      validation: validation.error.details,
    };
  return { status: true };
};
