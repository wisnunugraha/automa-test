const {
  // Login Validations
  authValidations: { authLoginValidation, authRegisterValidation },
} = require("./validations");

const {
  Bcrypt: { PasswordEncrypt, PasswordCompare },
  JWT: { JWTGenerate },
  OutParser: { OutSuccess, OutError, OutUnAuthorization, OutValidation },
  Uuid: { uuids },
} = require("../../utils");
const { authService } = require("../../service");

class AuthController {
  loginAuthController = async (req, res, next) => {
    let validation = authLoginValidation(req.body);
    if (validation.status == false)
      return res.send(OutValidation(validation, null, "You got validation !"));

    let response = await authService.authLoginService(req.body);
    if (response.status == true)
      return res.send(OutSuccess(response.response, response.messages));
    return res.send(OutError(response.response, response.messages));
  };
  regiserAuthController = async (req, res, next) => {
    let validation = authRegisterValidation(req.body);
    if (validation.status == false)
      return res.send(OutValidation(validation, null, "You got validation !"));

    let response = await authService.authRegisterService(req.body);
    if (response.status == true)
      return res.send(OutSuccess(response.response, response.messages));
    return res.send(OutError(response.response, response.messages));
  };
}

module.exports = new AuthController();
