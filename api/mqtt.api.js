const MQTTPublisher = require("../config/mqtt.config");

exports.MQTTPublishAPI = async ({ topic, messages }) => {
  console.log(topic, messages);
  // MQTTPublisher.on("connect", () => {
  // console.log("Connected");

  MQTTPublisher.subscribe([topic], () => {
    console.log(`Subscribe to topic '${topic}'`);
    let stringfy = JSON.stringify(messages);
    MQTTPublisher.publish(
      topic,
      stringfy,
      { qos: 0, retain: false },
      (error) => {
        if (error) {
          console.error(error);
        }
      }
    );
    //   });
  });
};
